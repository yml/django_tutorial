from django_tutorial.settings import *   # pylint: disable=W0614,W0401

DEBUG = True
TEMPLATE_DEBUG = DEBUG


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'django_tutorial2',
        'USER': 'dbuser2',
        'PASSWORD': 'dbuser2',
        'HOST': '127.0.0.1',
        'PORT': 5432
    }
}

WSGI_APPLICATION = 'django_tutorial.wsgi.dev.application'
