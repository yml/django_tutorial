from django.conf.urls.defaults import patterns, url, include

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    #(r'^$ ', ),
    (r'^polls/', include('django_tutorial.apps.polls.urls')),
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^admin/', include(admin.site.urls)),
)
